#######
Reports
#######

Durch das VIVA2-Addon werden mehrere Reports installiert, die der Report-Kategorie "IT-Grundschutz" zugeordnet werden. Dies umfasst aktuell vier "normale" Reports:

**Meine mir zugewiesenen Anforderungen**
    Dieser Report zeigt alle noch offenen Anforderungen an, die im IT-Grundschutz-Check dem aktuell eingeloggten User zugewiesen sind (als "zuständig für die Umsetzung"), sortiert nach geplanten Umsetzungsdatum.
**Report: Fehlende Verantwortlichkeiten**
    Dieser Report zeigt alle direkt einem Informationsverbund zugewiesene Zielobjekte/Zielobjektgruppen an, die *keine* Kontaktzuweisung mit einer der folgenden Rollen hat:

    * Service-Manager
    * IT-GS: Informationssicherheitsbeauftragter (ISB)
    * IT-GS: Grundsätzlich zuständig
    * IT-GS: Fachverantwortliche
    * Administrator
**Report: Verantwortlichkeiten**
    Dieser Report zeigt alle direkt einem Informationsverbund zugewiesene Zielobjekte/Zielobjektgruppen mit deren Kontaktzuweisung an, die eine der folgenden Rollen hat:

    * Service-Manager
    * IT-GS: Informationssicherheitsbeauftragter (ISB)
    * IT-GS: Grundsätzlich zuständig
    * IT-GS: Fachverantwortliche
    * Administrator
**Report: Von Informationsverbünden abgedeckt**
    Dieser Report zeigt alle Objekte -sortiert nach Informationsverbund- an, die entweder direkt einem Informationsverbund zugeordnet sind oder die Mitglieder einer Zielobjektgruppe sind, die einem Informationsverbund zugeordnet ist.


Die sechs variablen Reports sind alle über gleichnamige benutzerdefinierte Kategorien beim `Informationsverbund <objekttypen.html#objekttyp-informationsverbund>`_ eingebunden und dort beschrieben.