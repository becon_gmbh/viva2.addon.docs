##########
Einführung
##########

.. image:: img/Lizenzierter_GS_RGB.jpg
     :class: floatright

Das VIVA2 Add-on ist eine technisch und inhaltlich modernisierte Version des VIVA Add-on, die sich an den BSI IT-Grundschutzstandards 200-1 und 200-2 orientiert. Wie auch das vielfach bewährte VIVA Add-On unterstützt Sie das neue VIVA2 beim Aufbau eines Managementsystems für die Informationssicherheit (ISMS) nach der modernisierten BSI IT-Grundschutz-Methodik.

* Aufbau eines ISMS nach BSI-Standards 200-1 und 200-2, nahtlos in die CMDB integriert
* Import der offiziellen Bausteine aus dem BSI IT-Grundschutz-Kompendium
* Zuordnen von Verantwortlichkeiten
* Verwaltung der Anforderungen aus den Bausteinen
* Übersichtliche Verwaltung und Darstellung des IT-Grundschutz-Check
* Erstellung von Berichten und grafischen Übersichten
* Dokumentation des Schutzbedarfs mit visueller Darstellung der Schutzbedarfsvererbung
* Assistentengestützte Datenmigration von VIVA1 nach VIVA2

.. note::
    Wenn nicht anders erwähnt, bezieht sich diese Dokumentation immer auf die aktuellste Version des VIVA2-Add-ons.
