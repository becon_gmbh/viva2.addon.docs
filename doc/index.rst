######################################################
Willkommen zur Dokumentations des i-doit Add-Ons VIVA2
######################################################

Das VIVA2 Add-On unterstützt beim Aufbau und der Dokumentation eines ISMS (Information Security Management Systems) nach den neuen BSI IT-Grundschutz-Standards 200-1 und 200-2.

------------

.. toctree::
    :maxdepth: 1
    :caption: Erste Schritte
    
    einfuehrung
    systemvoraussetzungen
    installation
    einrichtung
    update
    profile
    migration


.. toctree::
    :maxdepth: 1
    :caption: Funktionsübersicht
    
    uebersicht
    objekttypen
    strukturanalyse
    schutzbedarfsfeststellung
    modellierung
    it-grundschutz-check
    reports
    

.. toctree::
    :maxdepth: 1
    :caption: Historie
    
    changelog 


##################
License
##################

`becon`_ © 2013-2020 becon GmbH

.. _becon: LICENSE.html