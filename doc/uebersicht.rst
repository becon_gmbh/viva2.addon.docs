#########
Übersicht
#########

Durch das VIVA2-Addon wird eine neue Objekttypgruppe "IT-Grundschutz" erstellt. Dieser Objekttypgruppe werden sechs neue Objekttypen zugeordnet:

* `Baustein <objekttypen.html#objekttyp-baustein>`_
* `Anforderung <objekttypen.html#objekttyp-anforderung>`_
* `Gefährdung <objekttypen.html#objekttyp-gefahrdung>`_
* `Informationsverbund <objekttypen.html#objekttyp-informationsverbund>`_
* `Zielobjektgruppe <objekttypen.html#objekttyp-zielobjektgruppe>`_
* `Kommunikationsverbindung <objekttypen.html#objekttyp-kommunikationsverbindung>`_


Die Objekte der drei Objekttypen Baustein, Anforderung und Gefährdung werden durch den Import des Grundschutzkompendiums angelegt und können natürlich auch problemlos durch eigene Bausteine, Anforderungen und Gefährdungen erweitert werden.


Bei der Definition des Informationsverbundes (oder auch mehrerer Informationsverbünde) und im Rahmen der :doc:`strukturanalyse` werden die Objekte der Objekttypen Informationsverbund, Zielobjektgruppe und Kommunikationsverbindung angelegt.

.. image:: img/objekttypen.png

In der :doc:`schutzbedarfsfeststellung` können die Schutzbedarfe definiert und begründet werden. Außerdem kann die Vererbung des Schutzbedarfs in der grafischen Darstellung nachvollzogen und verändert werden.


Im Rahmen der :doc:`modellierung` müssen dem Informationsverbund und den Zielobjekten/Zielobjektgruppen die entsprechenden Bausteine zugeordnet und den Bausteinen die Hauptansprechpartner zugewiesen werden.


Der :doc:`it-grundschutz-check` liefert eine Übersicht über die Anforderungen, die an den zugewiesenen Bausteinen hängen, unterteilt nach Basisanforderungen, Standardanforderungen und Anforderungen bei erhöhtem Schutzbedarf. Zu den Anforderungen können u.a. Umsetzungsstand, -datum und Verantwortlicher gepflegt werden.


Das VIVA2-Addon installiert mehrere :doc:`reports` zur erweiterten Auswertung und Dokumentation des ISMS.