##############################
Aktualisierung des Kompendiums
##############################

Der Import einer neuen Version des IT-Grundschutz-Kompendiums funktioniert prinzipiell genauso wie der Import während der Ersteinrichtung. Zum Import einer neuen Version des Kompendiums geht man auf |pfeil| Extras |pfeil| IT-Grundschutz und dort auf den ersten Punkt |pfeil| Kompendium.

Ein Klick auf den "Aktualisieren"-Button öffnet ein Popup, in dem man über den "Durchsuchen"-Button die ZIP-Datei vom IT-Grundschutz-Kompendiums auswählen muss. Nach dem Auswählen wird die Datei automatisch hochgeladen. Ein grüner Balken signalisiert, dass der Upload erfolgreich war.

.. image:: img/update_1.jpg

Ein Klick auf den "Aktualisieren"-Button startet den initialen Aktualisierungsassistenten. Dieser vergleicht die installierten Bausteine mit denen im hochgeladenen Grundschutzkompendiums und fragt, welche Elemente importiert werden sollen und welche nicht. Die Auflistung ist unterteilt in die Abschnitte "Neue Bausteine", "Geänderte Bausteine" und "Benutzerdefinierte Bausteine". Standardmäßig werden alle Bausteine, die im hochgeladenen Kompendium enthalten, sowie alle benutzerdefinierten Bausteine zur Übernahme markiert. Der Klick auf den "Weiter"-Button führt zur gleichen Übersicht, nur mit Gefährdungen, ein weiterer Klick auf den "Weiter"-Button führt dann zu den Anforderungen. Wenn man mit den Anforderungen fertig ist, startet der Klick auf den "Übernehmen"-Button den eigentlichen Importvorgang. Dieser Importvorgang kann mehrere Minuten dauern. Sobald sich das Popup automatisch schließt, ist der Aktualisierungevorgang abgeschlossen und auf der Übersichtsseite ist unter "Letzte Aktualisierung am" der Zeitstempel des Imports eingetragen.

.. image:: img/update_2.jpg


.. note::
    Alle Bausteine, Gefährdungen und Anforderungen, die nicht mit einem Häkchen in der ersten Spalte versehen werden, werden nicht übernommen. Für bereits vor dem Import vorhandene Elemente bedeutet dies, dass die jeweiligen Objekte automatisch archiviert werden.



.. |pfeil| unicode:: U+23F5