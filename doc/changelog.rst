#########
Changelog
#########


*************
Version 3.2.4
*************

18.04.2024

Deutsch
=======
::


	[Verbesserung]  Es gibt zwei neue Attribute, um den vererbten Schutzbedarf bzw. die Schutzbedarfsvererbung im Reportmanager ausgeben zu können
	[Verbesserung]  Im CSV-Export der grafischen Schutzbedarfsvererbung wird das Objekt, von dem der Schutzbedarf vererbt wird, ausgegeben
	[Verbesserung]  Im Bearbeitungsformular von Anforderungen im IT-Grundschutz-Check wird jetzt der Anforderungstext angezeigt
	[Verbesserung]  Neuer Variabler Report "IT-Grundschutz-Check (vollständig)" wird installiert, um alle Attribute der Kategorie "IT-Grundschutz-Check" anzeigen und exportieren zu können
	[Verbesserung]  Englische Übersetzungen verbessert
	[Änderung]      Die nicht mehr supportete Kategorie "Realisierungsplan" wird nicht mehr mitinstalliert und wird deinstalliert, wenn nicht in Benutzung
	[Bug]           Beim Hinzufügen einiger Spalten in der Objektlistenansicht kam es zu einer Fehlermeldung, weil das JSON-Format inkorrekt war
	[Bug]           Die Schutzbedarfe wurden nicht immer korrekt vererbt
	[Bug]           Bei der Konfiguration von Anforderungen in der Listenansicht vom IT-Grundschutz-Check kam es zu SQL-Fehlern
	[Bug]           Die Attribute "Wiederkehrende Kosten" und "Wiederkehrende Kosten (Intervall)" der Kategorie IT-Grundschutz-Check waren nicht im Reportmanager auswählbar
	

English
=======
::


	[Improvement]   Two new attribute in report manager available to report the inherited protection needs resp. the protection needs inheritance
	[Improvement]   In the CSV export of the graphical protection needs inheritance, the object from which the protection requirement is inherited is output
	[Improvement]   In the edit form of the requirements of the IT-Grundschutz-Check the requirement description is shown
	[Improvement]   New variable report available to be able to show and export all attributes of the category IT-Grundschutz-Check
	[Improvement]   Improved english translations
	[Change]        The category realization plan, which is no longer supported, won't be installed anymore and will be uninstalled if not in use
	[Bug]           Adding some columns in the object list view resulted in an error message because the JSON format was incorrect
	[Bug]           The protection needs were not always inherited correctly
	[Bug]           SQL errors occurred when configuring requirements in the list view of the IT-Grundschutz check
	[Bug]           The attributes "Recurring costs" and "Recurring costs (interval)" of the IT-Grundschutz check category could not be selected in the report manager

*************
Version 3.2.3
*************

25.05.2023

Deutsch
=======
::


	[Verbesserung]  Der JSON- und CSV-Export berücksichtigen jetzt auch die gesetzten Filter
	[Verbesserung]  In der Listenansicht der Infrastrukturanalyse wird jetzt auch die Objekt-ID angezeigt
	[Verbesserung]  Im CSV-Export werden in der "Verbunden mit"-Spalte Objekt-ID und Objekt-Titel mit angezeigt
	[Verbesserung]  Alphabetische Sortierung der Zielobjekte/-objektgruppen in der Filterung der Schutzbedarfsvererbung
	[Verbesserung]  Attribut "Abgrenzung" antsprechend dem Grundschutzkompendiums umbenannt nach "Abgrenzung und Modellierung"
	[Verbesserung]  "Basisabsicherung erreicht" erscheint jetzt auch, wenn Anforderungen als entbehrlich markiert sind
	[Verbesserung]  Anpassung der Optik und der Iconsets an das neue i-doit Layout
	[Bug]           Bei Änderung des Schutzbedarfes wurden keine Logbucheinträge erstellt
	[Bug]           Die Installationroutine des Add-Ons war nicht kompatibel mit i-doit Version >= 23 
	[Bug]           In der Kategorie IT-Grundschutz-Check war an zwei Stellen der Standardname der i-doit-Datenbank hardcodiert
	[Bug]           In der Kategorie IT-Grundschutz-Check wurde bei "Umsetzung bis" die Uhrzeit mit angezeigt
	[Bug]           In der Kategorie Schutzbedarf kam es in idoit 25 zu einem Server error
	

English
=======
::


	[Improvement]   The JSON and CSV export now also consider the applied filters.
	[Improvement]   The object ID is now displayed in the list view of the infrastructure analysis.
	[Improvement]   The CSV export now includes the object ID and object title in the "Connected with" column.
	[Improvement]   Alphabetical sorting of target objects/object groups in the protection inheritance filtering.
	[Improvement]   Renamed attribute "Differentiation" to "Differentiation and modelling" according to the IT-Grundschutz Compendium.
	[Improvement]   "Basisabsicherung erreicht" now appears even when requirements are marked as dispensable.
	[Improvement]   Adjustment of the appearance and icon sets to the new i-doit layout.
	[Bug]           No log entries were created when changing the protection requirement.
	[Bug]           The installation routine of the add-on was not compatible with i-doit version >= 23.
	[Bug]           In the IT-Grundschutz-Check category, the standard name of the i-doit database was hardcoded in two places.
	[Bug]           In the IT-Grundschutz-Check category, the time was displayed for the implementation date 
	[Bug]           In the Protection requirement category an server error occured in idoit 25

*************
Version 3.2.2
*************

09.01.2023

Deutsch
=======
::


	[Neue Funktion] Die Objekte/Knoten in der grafischen Schutzbedarfsfeststellungen lassen sich jetzt auch vertikal verschieben
	[Verbesserung]  Weitere optische Anpassungen an das neue i-doit Design
	[Verbesserung]  Die Höhe der Objekte/Knoten passt sich jetzt der Anzahl der konfigurierten (und vererbten) Grundwerte an
	[Verbesserung]  Das Attribut "Verantwortung" aus der Kategorie IT-Grundschutz-Check kann jetzt im Report ausgewählt werden
	[Verbesserung]  Die Kategorie "Klassifikation von Informationen" kann jetzt im Report ausgewählt werden
	[Verbesserung]	
	[Bug]           Das Anlegen, Bearbeiten und Verwenden von Grundschutzprofilen war nicht PHP 8 kompatibel
	

English
=======
::


	[New function]  The objects/nodes of the visual protection requirement assessment can be dragged now in vertical direction too 
	[Improvement]   More visual adaptions to the new i-doit layout
	[Improvement]   The height of the obejcts/nodes adapt now to the number of configured (and inherited) basic values
	[Improvement]   The attribute "Contact person" of the category IT baseline protection can be chosen in the report manager now
	[Improvement]   The category "Classification of information" can be chosen in the report manager now
	[Bug]           Creating, editing and the usage of IT baseline protection templates were not compatible with PHP 8

*************
Version 3.2.1
*************

12.09.2022

Deutsch
=======
::


	[Verbesserung]  Kompatibilität zu i-doit 1.19
	[Verbesserung]  Kompatibilität zu PHP 8.0
	[Verbesserung]  Optische Anpassungen an neues Layout von i-doit 1.19
	[Bug]           In der Kategorie "Bausteine" wurden die zugewiesenen Zielobjekte/-objektgruppen nicht angezeigt
	[Bug]           Die Objekttypicons von "Kommunikationsverbindung" und "Gefährdung" konnten nicht geladen werden
	[Bug]           Beim Editieren der Kategorie "IT-Grundschutz-Check" gingen die Verantwortlichen verloren, wenn man aus der "View"-Sicht kam


English
=======
::


	[Improvement]   Compatibility to i-doit 1.19
	[Improvement]   Compatibility to PHP 8.0
	[Improvement]   Optical improvements to fit new layout of i-doit 1.19
	[Bug]           In category "Modules" the assigned target objects/object group were not shown
	[Bug]           The object type icons of "Communication link" and "Threat" could'nt be loaded
	[Bug]           The responsible contacts were lost, if editing the category "IT baseline check" and comming from "View" view

***********
Version 3.2
***********

25.04.2022

Deutsch
=======
::


	[Neue Funktion] Unterstützung des neuen Formats des IT-Grundschutzkompendiums 2022
	[Neue Funktion] Im IT-Grundschutz-Check können die Bausteine und Absicherungsarten auf- und zugeklappt werden, der Zustand wird in den persönlichen Usersettings gespeichert
	[Verbesserung]  Bearbeitung des Schutzbedarfes am Objekt jetzt per Inline-Editung möglich
	[Verbesserung]  Die Editierrechte für die Kategorie Schutzbedarf werden jetzt überall berücksichtigt
	[Verbesserung]  Verlinkung des Baustein-Objektes im IT-Grundschutz-Check
	[Verbesserung]  Beim Import des Kompendiums wird die Schicht im Baustein mit übernommen 
	[Verbesserung]  Bereinigung fehlerhafte Einträge in der Kategorie Schutzbedarf
	[Änderung]      Die Buttons "Übernehmen" und "Anpassen" in der Kategorie Schutzbedarf wurden entfernt
	[Bug]           Geänderte Bausteinnamen wurden beim Kompendiums-Import nicht im Objektnamen aktualisiert
	[Bug]           Listeneditierung des Schutzbedarfs konnte zu fehlerhaften Datenbankeinträgen führen
	[Bug]           Beim Import des Kompendiums wurden teilweise keine Logbucheinträge geschrieben
	[Bug]           Rechtschreibfehler in deutscher Übersetzung wurde behoben
	[Bug]           Der Menüpunkt "Schutzbedarfsfeststellung" überprüfte das falsche Benutzerrecht
	[Bug]           Es kam zu Fehlern beim Import des Kompendiums, wenn eigene Bausteine, Anforderungen oder Gefährdungen vorhanden waren
	[Bug]           Die Umsetzung der Anforderungen im IT-Grundschutz-Check verloren beim Speichern Formatierungen und Zeilenumbrüche


English
=======
::


	[New function]  Support of new format of the IT-Grundschutzkompendiums 2022
	[New function]  Modules and requirement groups can now be folded and unfolded in the it baseline protection check, the status is saved in the personal user settings
	[Improvement]   Editing of the protection requirement of an object now with inline editing possible
	[Improvement]   Respect edit right of category protection requirement everywhere
	[Improvement]   Module objects are now linke din the IT baseline protection check
	[Improvement]   The layer of the module is now taken over during import of the Kompendium
	[Improvement]   Clean up wrong database entries of category protection requirement
	[Change]        The buttons "Accept" and "Adapt" are removed from category protection requirement
	[Bug]           Changes module names were not updated during import of the Kompendium
	[Bug]           List edit of protection requirements possibly led to wrong database entries
	[Bug]           During import of the Kompendium sometimes no log book entries were wrote
	[Bug]           Wrong spelling in german translation was fixed
	[Bug]           The menu entry "Assessment of protection requirements" checked wrong user permissions
	[Bug]           The import of the Kompendium led to errors, if custom modules, requirements or threats were present
	[Bug]           The realization of requirements in the IT baseline protection check lost its formattings and line breaks when saving

*************
Version 3.1.3
*************

11.02.2021

Deutsch
=======
::


	[Bug]           Die Schutzbedarfsfeststellung funktionierte auf einigen Systemen mit i-doit 1.16 nicht


English
=======
::


	[Bug]           The protection requirements didn't work on some systems with i-doit 1.16

*************
Version 3.1.2
*************

04.02.2021

Deutsch
=======
::


	[Verbesserung]  Neuer Benutzerabhängige Report "Meine mir zugewiesenen Anforderungen"
	[Verbesserung]  Anpassung der primären Zugriffs-URL an den neuen Aufbau der BSI-Webseite
	[Verbesserung]  Neue Icons für das Extra-Menü
	[Verbesserung]  VIVA2-Objekttypen-Bilder und -icons in den i-doit-Bilderordner verschoben
	[Verbesserung]  "Umsetzungsdatum" umbenannt nach "Umsetzung bis"
	[Bug]           Die Beschreibung des VIVA2-Upload-Ordner in den Mandenteneinstellungen war falsch
	[Bug]           Das Attribut "Beschreibung" in der Kategorie IT-Grundschutz (Informationsverbund) -> IT-Grundschutz-Check wurde nicht gespeichert


English
=======
::


	[Improvement]   New user-dependent report "Requirements assigned to mir"
	[Improvement]   Adapt primary access URL to new layout of BSI website
	[Improvement]   New icons in the "Extra" menu
	[Improvement]   Moved VIVA2 object images and icons into i-doit image folder
	[Improvement]   Renamed "Implementation date" to "Implementation until"
	[Bug]           The label of the viva2 upload folder in the tenant settings was wrong
	[Bug]           The attribute "description" of the category IT baseline prootection (scope) -> IT baseline protection check wasn't saved

*************
Version 3.1.1
*************

10.07.2020

Deutsch
=======
::


	[Verbesserung]  Anpassungen für den Import des Grundschutz-Kompendiums 2020
	[Verbesserung]  Die Reports "Verantwortlichkeiten" und "Fehlende Verantwortlichkeiten" überarbeitet und verbessert
	[Verbesserung]  Doppelt vorhandene Rollen konvertiert und entfernt
	[Bug]           Kategorie IT-Grundschutz Check funktionierte nicht wenn die Datenbank nicht „idoit_data“ heißt
	[Bug]           Profile ließen sich nicht anlegen, wenn der Ordner "exports" nicht im VIVA2 Addon Ordner vorhanden war
	[Bug]           Die Druckansicht des variablen Reports "IT-Grundschutz-Check" blieb leer

English
=======
::


	[Improvement]   The Kompendium 2020 can now be imported
	[Improvement]   The reports "Responsibilities" and "Missing Responsibilities" are revised and improved
	[Improvement]   Duplicate roles are converted and removed
	[Bug]           Category "IT-Baseline protetion chec" didn't work when the name of the tenant database wasn't "idoit_data"
	[Bug]           Profiles could not be created when the folder "exports" didn't exist in viva2 addon folder
	[Bug]           The print view of variable report "IT baseline protection check" was empty

***********
Version 3.1
***********

12.05.2020

Deutsch
=======
::

	[Neue Funktion] Neuer Objekttyp "Kommunikationsverbindung"
	[Neue Funktion] Vererbung des Schutzbedarfs über die neue Beziehung "Kommunikationsverbindung"
	[Neue Funktion] Die grafische Darstellung der Schutzbedarfsfeststellung kann jetzt gefiltert werden
	[Neue Funktion] Die Begründung für die Schutzbedarfsfeststellung kann in der grafischen Darstellung eingetragen werden
	[Verbesserung]  Die Begründung für die Schutzbedarfsfeststellung ist jetzt auch im CSV-Export enthalten
	[Verbesserung]  Rollen, die durch das Grundschutzkompendium hinzugefügt werden, werden jetzt auch deinstalliert
	[Verbesserung]  Die aktualisierte Dokumentation zum VIVA2-Addons ist ab jetzt hier zu finden: https://viva2.readthedocs.io
	[Bug]           Übersetzungen für "SVG-Datei" und "PNG-Datei" sind in der 1.14.1 nicht mehr vorhanden und werden jetzt durch das Addon geliefert
	[Bug]           Übersetzung vom "Bitte warten" Overlay der Schutzbedarfsfeststellung wurde nur angezeigt, wenn das Raumplan-Addon installiert ist
	[Bug]           Die Schutzbedarfsfeststellung zeigte einen Fehler an, wenn die angezeigten Zielobjekte keine Verbindung haben.


English
=======
::

	[New feature]   New object type "communication link"
	[New feature]   The protection requirements will be inherited over the new relation "communication link"
	[New feature]   It is now possible to set a filter in the graphical representation of the assessment of protection requirements
	[New feature]   The reason for the assessment of the protection requirements can now be edited in the graphical representation
	[Improvement]   The reason for the assessment of the protection requirements in included now in the CSV export
	[Improvement]   Contact roles, which will be imported by the IT-Grundschutz Compendium, will now be removed during deinstallation of the addon
	[Improvement]   The updated documentation for the VIVA2 addon can now be found here: https://viva2.readthedocs.io (only in german yet)
	[Bug]           Translations for "CSV file" and "PNG file" were not available in 1.14.1 anymore
	[Bug]           Translation of the "Please wait" Overlay in the assessment of protection requirements was only shown, if floorplan addon was installed
	[Bug]           The assessment of the protection requirements has shown an error, if the shown target objects didn't have any links


***********
Version 3.0
***********

31.03.2020

Deutsch
=======
::

	[Neue Funktion] Der Umsetzungsstatus der Anforderungen wird jetzt an den Zielobjekten/-gruppen gespeichert
	[Neue Funktion] Komplette Überarbeitung des "IT-Grundschutz-Check"
	[Neue Funktion] Neue Kategorie "Prozessbausteine" am Informationsverbund
	[Verbesserung]  Die IT-GS-Kategorien am Informationsverbund sind in einem Ordner zusammengefasst
	[Verbesserung]  Überarbeitung aller mitgelieferten Reports, Entfernen der Reports "Nicht vom Informationsverbund abgedeckt" und "Nicht von Informationsverbünden abgedeckt"
	[Verbesserung]  Umbenennung von Zielgruppen und Zielobjektgruppen
	[Verbesserung]  Umbenennung der Rolle "Verantwortliche Stelle" zu "Grundsätzlich zuständig"
	[Verbesserung]  Alle bei der Installations des VIVA2-Addon hinzugefügten Rollen erhalten den Prefix "IT-GS:"
	[Verbesserung]  Vorbereitung für nächste Version zum Editieren der Begründung bei der Schutzbedarfsfeststellung
	[Bug]           Reports mit Kategorien aus VIVA2 führen zu einem SQL-Fehler (behoben ab 1.14.1)
	[Bug]           Nach dem Import des Kompendiums wird der Suchindex für die neuen/geänderten Objekte nicht neu erstellt
	[Bug]           VIVA2-Deinstallation lässt die Objekttypgruppe im Menü 
	[Bug]           Fehler bei der Listeneditierung der Kategorie IT-Grundschutz-Check
	[Bug]           Der "Durchsuchen"-Button fehlt in ->Informationsverbund->Bereinigter Netzplan


English
=======
::

	[New feature]   The implementation status of the requirements is now saved on the target objects/groups
	[New feature]   Complete revision of the "IT baseline protection check" category
	[New feature]   New category "process modules" at the object type "scope"
	[Improvement]   The addon categories in the object type "scope" are summarized in a folder
	[Improvement]   Revision of all supplied reports, removal of the reports "Not covered by scope" and "Not covered by scopes"
	[Improvement]   Renaming target groups into target object groups
	[Improvement]   Renaming the role "responsible person" to "basically responsible"
	[Improvement]   All roles added during the installation of the VIVA2 addon are given the prefix "IT-BP:"
	[Improvement]   Preparation for the next version to edit the reason in the asessment of protection requirements
	[Bug]           Reports containing categories from VIVA2 led to an SQL error (only working in 1.14.1)
	[Bug]           After importing the compendium, the search index for the new/changed objects wasn't recreated
	[Bug]           VIVA2 deinstallation leaves the object type group in the menu
	[Bug]           Error when using the list edit for the category IT baseline protection
	[Bug]           The "Browse" button is missing in Scope -> IT baseline protection (Scope)-> Adjusted network plan


*************
Version 2.0.3
*************

09.01.2020

Deutsch
=======
::

[Neue Funktion] Schutzbedarfsfeststellung: Neue Exportmöglichkeit ím CSV Format
[Neue Funktion] Schutzbedarfsfeststellung: Neue Exportmöglichkeit ím JSON Format
[Verbesserung]  Schutzbedarfsfeststellung: Services und deren Beziehungen werden nun berücksichtigt
[Verbesserung]  Vorbereitung auf kommendes Major-Realease: Anforderungstyp wird beim Import des Kompendiums an der Anforderung gespeichert
[Bug]           Englische Übersetzungen fehlten teilweise
[Bug]           Beim Aktualisieren des Grundschutz-Kompendiums sollte es "Benutzerdefinierte Anforderungen" und nicht "Benutzer-definierte Anforderungen" heißen
[Bug]           Reports wurden bei der Installation nicht korrekt angelegt
[Bug]           Addon wird nicht ordnungsgemäß deinstalliert

English
=======
::

[New feature]   Asessment of protection requirements: New export option in CSV format
[New feature]   Asessment of protection requirements: New export option in JSON format
[Improvement]   Asessment of protection requirements: Services and their relationships are now taken into account
[Improvement]   Preperation for nect major release: Type of requirement is saved on requirement when importing the IT baseline protection kompendium
[Bug]           Some english translations were missing
[Bug]           Small fix in german translation
[Bug]           Reports were not created correctly during installation
[Bug]           Uninstallation of addon does not work correctly


*************
Version 2.0.2
*************

19.09.2019

Deutsch
=======
::

[Verbesserung]  Neue Icons und Bilder für VIVA2-Objekttypen
[Bug]           Reports werden bei der Neuinstallation in i-doit 1.13.1 (und höher) in die falsche Datenbank geschrieben


English
=======
::

[Improvement]   New icons and images for VIVA2 object types
[Bug]           Reports are stored in the wrong database during a new installation in i-doit 1.13.1 (or higher)


*************
Version 2.0.1
*************

Deutsch
=======
::

[Bug]           YAML ungültig
[Bug]           Doppelte Anzeige von Informationsverbunde in der Migration
[Bug]           Gefährdungen im IT-Grundschutz werden nicht mehr angezeigt
[Bug]           Schutzbedarfsfeststellung lässt sich nicht direkt nach öffnen des Add-Ons aufrufen
[Bug]           Migration von VIVA auf VIVA2 bei leeren Inhalten teilweise fehlerhaft
[Bug]           In Kategorie Zugwiesene Anforderungen wird eine Fehlermeldung ausgegeben wenn man auf einen Eintrag klickt
[Aufgabe]       VIVA 2 PHP 7.2 Kompabilität


English
=======
::

[Bug]           Invalid YAML
[Bug]           Double display of information networks in the migration
[Bug]           Threats in basic IT protection are no longer displayed
[Bug]           Schutzbedarfsfeststellung doesn't open when selected right after opening the add-on
[Bug]           Migration from VIVA to VIVA2 sometimes faulty when using empty content
[Bug]           In category "Assigned requirements" an error message is displayed when clicking on an entry
[Task]          VIVA 2 PHP 7.2 Compability

