###########
Einrichtung
###########

Bevor man mit dem VIVA2-Addon ein ISMS nach BSI Grundschutz-Methodik aufbauen kann, muss das aktuelle Grundschutzkompendium installiert. Dazu geht man im Menü auf 
|pfeil| Extras |pfeil| IT-Grundschutz und dort auf den ersten Punkt |pfeil| Kompendium.

.. warning::
	Das IT-Grundschutz-Kompendium ab Edition 2022 kann erst ab VIVA2 Version 3.2 importiert werden. Bitte auch unbedingt die :doc:`systemvoraussetzungen` beachten.

.. image:: img/einrichtung_2.jpg

*Das Grundschutz-Kompendium zum Import in das VIVA2-Addon kann man sich im i-doit Kundenportal* (https://login.i-doit.com) *herunterladen.*

Ein Klick auf den "Importieren"-Button öffnet ein Popup, in dem man über den "Durchsuchen"-Button die ZIP-Datei vom IT-Grundschutz-Kompendiums auswählen muss. Nach dem Auswählen wird die Datei automatisch hochgeladen. Ein grüner Balken signalisiert, dass der Upload erfolgreich war.

.. image:: img/einrichtung_3.jpg

Ein Klick auf den "Importieren"-Button startet den initialen Import des IT-Grundschutz-Kompendiums. Dieser Importvorgang kann mehrere Minuten dauern. Sobald sich das Popup automatisch schließt, ist der Importvorgang abgeschlossen und auf der Übersichtsseite ist unter "Importiert am" der Zeitstempel des Imports eingetragen.


Die technische Einrichtung des VIVA2-Addons ist jetzt abgeschlossen.


.. note::
    Durch den initialen Import des IT-Grundschutz-Kompendiums des BSI werden mehrere lizenzpflichtige Objekte angelegt. Die genaue Anzahl ist abhängig von der Version des Kompendiums. Bei der aktuellen Edition 2023 sind das:

    ============== ==============
    Objekttyp      Anzahl Objekte
    ============== ==============
    Anforderung    2109 (davon 288 entfallen*)         
    Baustein       111            
    Gefährdung     691           
    ============== ============== 

    \* *Mit dem Import des Grundschutzkompendiums werden auch inzwischen obsolete Anforderungen mit importiert. Das BSI hat diese Anforderungen mit dem Titel "ENTFALLEN" versehen. Aus Gründen der Abwärtskompatibilität werden diese Anforderungen alle mit importiert, können aber problemlos hinterher alle gelöscht werden.*

.. |pfeil| unicode:: U+23F5